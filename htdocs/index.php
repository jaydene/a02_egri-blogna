<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>CS3140 project A02 for Jayden Egri-Blogna</title>
</head>
<body>
<style>

main #similar .box {
  width: 180px;
  border: solid 1pt #e7e7e7;
  padding: 0.5em;
  margin-right: 1em;
  float: left;
}

main #similar {
  width: 150px;
  border: solid 1pt #e7e7e7;
  padding: 10px 20px;
  margin-right: 1em;
  margin-left: 1em;
  float: left;
  text-align: center;
  margin: 0 15px 0 0;
}

.button {
  background-color: lightgrey;
    background-image: linear-gradient(white, lightgrey);
     border: solid 1pt black;
     border-radius: 5px;
     color: blue;
     text-align: center;
     text-decoration: none;
     display: inline-block;
     font-size: 14px;
     font-weight: bold;
     margin: 16px 14px;
     cursor: pointer;
     padding: 10px 70px;
}


ul {
  text-align: left;
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: white;
}
body{
      background-image: url('stucco.png');
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}

  div{
    list-style-type: none;
    margin: 50px;
    padding: 10px;
    overflow: hidden;
    border: 1px solid #e7e7e7;
    background-color: white;
  }

  .column {
    float: left;
    width: 225px;
    padding: 5px;
    text-align: center;
  }

  * {
    box-sizing: border-box;
  }


  body{
        background-image: url('stucco.png');
        font-family: "Helvetica Neue", "Helvetica", "Arial", "Sans-serif";
  }

  h1{
      margin: 50px;
  }

  h3{
    margin: 5px;
  }

  ul {
    text-align: left;
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: white;
    margin: 50px;
  }

  div {
    margin: 50px;
    padding: 10px;
    border: 1px solid #e7e7e7;
    background-color: white;
  }

  .topnav {
  overflow: hidden;
  background-color: #000;
  margin: 50px;
}

.topnav a {
  float: right;
  color: gray;
  text-align: right;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

  a{
    color: gray;
  }

  a:link {
  text-decoration: none;
  color: grey;
  }

  li {
    float: left;
  }

  li a {
    display: block;
    color: gray;
    text-align: left;
    padding: 14px 16px;
    text-decoration: none;
  }

  footer {
    text-align: center;
    padding: 10px;
    background-color: Black;
    color: gray;
    margin: 50px;
  }

  table, th, td {
      border: none;
    }

  </style>

    <header class="topnav">
        <a href="#my_account">My Account</a>
        <a href="#wish_list">Wish List</a>
        <a href="#shopping_cart">Shopping Cart</a>
        <a href="#checkout">Checkout</a>
    </header>

      <h1 style="text-align:left">Art Store</h1>

  <ul>
    <li><a href="index.html">Home</a></li>
    <li><a href="about.html">About Us</a></li>
    <li><a href="#art_works">Art Works</a></li>
    <li><a href="#artists">Artists</a></li>
  </ul>

  <div>
    <h2>Self-Portrait in Straw Hat</h2>
      <p>
        By <a href="https://www.nationalgallery.org.uk/paintings/international-womens-day-elisabeth-louise-vigee-le-brun", style="color:blue"; style="text-decoration:underline"><u>Louise Elisabeth Lebrun</u></a>
      </p>
      <img src="113010.jpg" alt="photo" width ="300" style="float:left; margin:20px 20px" align="left">
      The painting appears, after cleaning, to be an autograph replica of a picture, the original<br> of which was painted in Brussels in 1782 in free imitation of Ruebens's 'Chapeau de Paille', which<br> LeBrun had seen in Antwerp. It was exhibited in Paris in 1782 at the Salon de la<br> Correspondence.

  <br>
  <br>
  <p style="color:red;"font:"Arial";>$700</p>

  <div>
    <button class="button">Add to Wish List</button>
    <button class="button">Add to Shopping Cart</button>
  </div>



    <h3>Product Details</h3>
    <table>
      <tr>
        <td><b>Date:</b></td><td>1782</td>
      </tr>
      <tr>
        <td><b>Medium:</b></td><td>Oil on canvas</td>
      </tr>
      <tr>
        <td><b>Dimensions:</b></td><td>98cm x 71cm</td>
      </tr>
      <tr>
        <td><b>Home:</b></td><td><a href="https://www.nationalgallery.org.uk/", style="color:blue;", style="text-decoration:underline;"><u>National Gallery, London</u></a></td>
      </tr>
      <tr>
        <td><b>Genres:</b></td><td><a href="https://en.wikipedia.org/wiki/Realism_(arts)", style="color:blue;", style="text-decoration:underline;"><u>Realism</u>,<a href="https://en.wikipedia.org/wiki/Rococo", style="color:blue;", style="text-decoration:underline;"><u>Rococo</u></a></a></td>
      </tr>
      <tr>
        <td><b>Subjects:</b></td><td><a href="https://en.wikipedia.org/wiki/People", style="color:blue;", style="text-decoration:underline;"><u>People</u>,<a href="https://en.wikipedia.org/wiki/Arts", style="color:blue;", style="text-decoration:underline;"><u>Arts</u></a></a></td>
      </tr>
    </table>

    <p>LeBrun's original is recorded in a private <br>collection in France.</p>

    <h2>Similar Products</h2>

  <div class="row">
    <div class="column">
      <img src="116010.jpg" alt="Thistle" width = "150px">
        <br>
        <a href="https://en.wikipedia.org/wiki/Portrait_of_the_Artist_Holding_a_Thistle", style="color:blue"; style="text-decoration:underline"><u>Artist Holding a Thistle</u></a>
    </div>

    <div class="column">
        <img src="120010.jpg" alt="Eleanor" width = "150px">
          <br>
          <a href="https://en.wikipedia.org/wiki/Portrait_of_Eleanor_of_Toledo", style="color:blue"; style="text-decoration:underline"><u>Portrait of Eleanor of Toledo</u></a>
    </div>

    <div class="column">
      <img src="107010.jpg" alt="Pompadour" width = "150px">
        <br>
        <a href="https://en.wikipedia.org/wiki/Madame_de_Pompadour", style="color:blue"; style="text-decoration:underline"><u>Madame de Pompadour</u></a>
    </div>

    <div class="column">
        <img src="106020.jpg" alt="Pearl" width = "150px">
          <br>
          <a href="https://en.wikipedia.org/wiki/Girl_with_a_Pearl_Earring", style="color:blue"; style="text-decoration:underline""><u>Girl with a Pearl Earring</u></a>
    </div>
  </div>


</body>

<footer>
All images are copyright to their owners. This is just a hypothetical site © 2014 Copyright Art Store
</footer>

</html>
