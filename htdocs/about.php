<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>CS3140 project A02 for Jayden Egri-Blogna</title>
</head>

<style>

  main #similar {
      width: 150px;
      border: solid 1pt #e7e7e7;
      padding: 10px 20px;
      margin-right: 1em;
      margin-left: 1em;
      float: left;
      text-align: center;
  }

  body{
        background-image: url('stucco.png');
        font-family: "Helvetica Neue", "Helvetica", "Arial", "Sans-serif";
  }

  h1{
      margin: 50px;
  }

  ul {
    text-align: left;
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: white;
    margin: 50px;
  }

  div{
    list-style-type: none;
    margin: 50px;
    padding: 10px;
    overflow: hidden;
    border: 1px solid #e7e7e7;
    background-color: white;
  }

  .topnav {
    overflow: hidden;
    background-color: #000;
    margin: 50px;
  }

  .topnav a {
    float: right;
    color: gray;
    text-align: right;
    padding: 14px 16px;
    text-decoration: none;
    font-size: 17px;
  }

  a{
    color: gray;
  }

  a:link {
  text-decoration: none;
  color: grey;
  }

  li {
    float: left;
  }

  li a {
    display: block;
    color: gray;
    text-align: left;
    padding: 14px 16px;
    text-decoration: none;
  }

  footer {
    text-align: center;
    padding: 10px;
    background-color: Black;
    color: gray;
    margin: 50px;
  }

  </style>

  <body>

    <header class="topnav">
        <a href="#my_account">My Account</a>
        <a href="#wish_list">Wish List</a>
        <a href="#shopping_cart">Shopping Cart</a>
        <a href="#checkout">Checkout</a>
    </header>

      <h1 style="text-align:left">Art Store</h1>

  <ul>
    <li><a href="index.html">Home</a></li>
    <li><a href="about.html">About Us</a></li>
    <li><a href="#art_works">Art Works</a></li>
    <li><a href="#artists">Artists</a></li>
  </ul>

  <br>



<div>
  <h2>About Us</h2>
    <p>
      This assignment was created by Jayden Egri-Blogna.<br><br> It was created for CS3140 at Bowling Green State University
    </p>
</div>

<br>

<footer>
All images are copyright to their owners. This is just a hypothetical site © 2014 Copyright Art Store
</footer>

</body>
</html>
